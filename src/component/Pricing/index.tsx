'use client'
import Image from 'next/image'
import './pricing.scss'
import doller_one from '../../../public/svg/pricing-doller-one.svg'
import doller_two from '../../../public/svg/pricing-doller-two.svg'
import model_img_one from '../../../public/svg/modal-img-one.svg'
import model_img_two from '../../../public/svg/modal-img-two.svg'
import doc_img from '../../../public/svg/doc-img.svg'
import transaction_img from '../../../public/svg/transaction-img.svg'

const Pricing = () => {
    return (
        <>
            {/* Pricing Section */}
            <hr className="pricing-border" />
            <div className="pricing-section">
                <div className="container">
                    <div className="main-title">
                        <h1>Pricing</h1>
                    </div>
                    <div className="pricing-card-one">
                        <h6 className="ornage-color">Free Registration</h6>
                        <ul>
                            <li className="pricing-card-one-text" style={{fontWeight: 400}}>
                                Dufther does not charge Service Fees to the Job
                                Seekers. It is our endeavor to support the
                                Candidates to Upskill, and gain the required
                                knowledge to deliver at workplace. Candidates
                                will have opportunities to grow their Job
                                Skills, move across different industries, and
                                get meaningful employment.
                            </li>
                        </ul>
                    </div>
                    <div className="pricing-card-two">
                        <h6 className="ornage-color">Value Added Services</h6>
                        <ul>
                            <li style={{fontWeight: 400}}>
                                For Organizations, we charge a very minimalistic
                                Pricing to cover our Operational, and
                                Infrastructure costs.
                            </li>
                        </ul>
                    </div>
                    <Image src={doller_two} alt="no image" />
                </div>
                <Image src={doller_one} alt="no image" />
            </div>

            {/* Pricing Models section */}
            <div className="pricing-models   ">
                <div className="main-title">
                    <h2>Pricing Models</h2>
                </div>
                <div className="row model-section-one">
                    <div className="col-12 col-sm-12 col-md-4 col-lg-6 col-xl-5 d-flex justify-content-center content-center order-category-2">
                        <Image
                            src={model_img_one}
                            alt="no image"
                            className="img-view "
                        />
                    </div>
                    <div className="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-7 ">
                        <div className="title-section ">
                            <Image
                                src={transaction_img}
                                alt="no image"
                                className="img-icon"
                            />
                            <h3 className="text-center mt-3">
                                Transaction Based{' '}
                                <span className="ornage-color ">Pricing</span>
                            </h3>
                        </div>
                        <ul>
                            <li>
                                Choose from a variety of Options to buy
                                different packages based on your Business needs.
                            </li>
                            <li>
                                Our Pricing works for an Individual Proprietor
                                Entity to a Large Corporate firm.
                            </li>
                            <li>
                                Please{' '}
                                <span className="ornage-color">
                                    <u>Login</u>
                                </span>{' '}
                                to view our various Pricing plans.
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row model-section">
                    <div className="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
                        <div className="title-section">
                            <Image
                                src={doc_img}
                                alt="no image"
                                className="img-icon"
                            />
                            <h3 className="mt-3 text-center">
                                Purchase a{' '}
                                <span className="ornage-color mrl">
                                    License
                                </span>
                            </h3>
                        </div>
                        <ul>
                            <li>
                                We operate on the Software-as-a-Service model
                                for roles such as Staffing Agencies, and
                                Skilling Partners. This means that any
                                improvements in our offerings will be available
                                to you without paying any additional charges.
                            </li>
                            <li>
                                As an initial Launch Offer, you can purchase a
                                license for one Year at a Highly Subsidized
                                rate, and experience our Site capabilities.
                            </li>
                        </ul>
                    </div>
                    <div className="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5 content-center  order-category-2">
                        <Image
                            src={model_img_two}
                            alt="no image"
                            className="img-view "
                        />
                    </div>
                </div>
            </div>

            {/* Bottom Card Section*/}

            <div className="pricing-card-three">
                <h6 className="ornage-color">Be Vigilant</h6>
                <ul>
                    <li style={{fontWeight: 300}}>
                        We do not have any other medium of payment other than
                        the links shared on our Portal. Please be aware of any
                        fraudulent emails or calls asking for Transfer of money,
                        or Confidential Information such as Bank Account, Credit
                        Card, Aadhaar, PAN details etc.
                    </li>
                </ul>
            </div>
        </>
    )
}

export default Pricing
