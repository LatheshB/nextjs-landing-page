const navItems = [
    { title: 'Home', route: '/' },
    { title: 'About Us', route: '/about-us' },
    { title: 'Pricing', route: '/pricing' },
    {
        title: 'Policies',
        route: 'https://portal.dufther.com/user-agreement/terms',
        openInNew: true,
    },
]

export { navItems }
