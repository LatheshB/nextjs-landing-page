'use client'
import Image from 'next/image'
import footer_img_1 from '@/images/footer_img_1.png'
import footer_img_2 from '@/svg/footer_img_2.svg'
import facebook_icon from '@/svg/facebook_icon.svg'
import facebook_icon_hover from '@/svg/facebook_icon_hover.svg'
import linkedin_icon from '@/svg/linkedin_icon.svg'
import linkedin_icon_hover from '@/svg/linkedin_icon_hover.svg'
import youtube_icon from '@/svg/youtube_icon.svg'
import youtube_icon_hover from '@/svg/youtube_icon_hover.svg'
import mail_icon from '@/svg/mail_icon.svg'
import './footer.scss'
import { useEffect, useState } from 'react'
import { Modal, Table } from 'react-bootstrap'
import { versionData } from './versionHistory'

const landingURL = process.env.NEXT_PUBLIC_LANDING_URL

export const Footer = () => {
    const year = new Date().getFullYear()
    const [show, setShow] = useState(false)
    const [windowSize, setWindowSize] = useState<any>()
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    useEffect(() => {
        setWindowSize(window.innerWidth)
        const handleWindowResize = () => {
            if (typeof window !== 'undefined') {
                setWindowSize(window.innerWidth)
            }
        }
        window.addEventListener('resize', handleWindowResize)

        return () => {
            window.removeEventListener('resize', handleWindowResize)
        }
    }, [])
    return (
        <>
            <footer className="footer">
                <div className="container">
                    <div className="row footer-menu-email justify-content-between align-items-center">
                        <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-4">
                            <div className="footer-social d-none d-sm-none d-md-block d-lg-block">
                                <div className="footer-social-icon d-flex align-items-center justify-content-start">
                                    <span className="white-color">
                                        Follow Us :{' '}
                                    </span>
                                    <ul className="d-flex align-items-center footer-ul">
                                        <li>
                                            <a
                                                href="https://www.facebook.com/Dufther"
                                                target="_blank"
                                            >
                                                <Image
                                                    src={facebook_icon}
                                                    alt="no image"
                                                    className="img-fluid social-svg"
                                                />
                                                <Image
                                                    src={facebook_icon_hover}
                                                    alt="no image"
                                                    className="img-fluid social-hover-svg"
                                                />
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://www.linkedin.com/company/dufther"
                                                target="_blank"
                                            >
                                                <Image
                                                    src={linkedin_icon}
                                                    alt="no image"
                                                    className="img-fluid social-svg"
                                                />
                                                <Image
                                                    src={linkedin_icon_hover}
                                                    alt="no image"
                                                    className="img-fluid social-hover-svg"
                                                />
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://www.youtube.com/channel/UCkMtaYSgsNNlD5V2gpF5wQg/?guided_help_flow=5"
                                                target="_blank"
                                            >
                                                <Image
                                                    src={youtube_icon}
                                                    alt="no image"
                                                    className="img-fluid social-svg"
                                                />
                                                <Image
                                                    src={youtube_icon_hover}
                                                    alt="no image"
                                                    className="img-fluid social-hover-svg"
                                                />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                            <div className="footer-second-link footer-link">
                                <ul className="d-flex flex-row align-items-center justify-content-evenly">
                                    {windowSize > 500 && (
                                        <>
                                            <li>
                                                <a
                                                    href={`${landingURL}/about-us`}
                                                    style={{
                                                        fontWeight: 600
        
                                                    }}
                                                >
                                                    Company
                                                </a>
                                            </li>
                                            <li>
                                                <a
                                                    href={`${landingURL}/user-agreement/terms`}
                                                    target="_blank"
                                                    style={{
                                                        fontWeight: 600
        
                                                    }}
                                                >
                                                    Policies
                                                </a>
                                            </li>
                                        </>
                                    )}
                                    <li
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <a
                                            href="https://docs.google.com/forms/d/e/1FAIpQLSc85VXvUCjl68ZJH0wIZhqvo5AhotFmhtFjKfoGsGooIzvbFg/viewform?embedded=true"
                                            target="_blank"
                                            style={{
                                                fontWeight: 600

                                            }}
                                        >
                                            Partner Us
                                        </a>
                                        {windowSize < 500 && (
                                            <a
                                                href="mailto:support@dufther.com"
                                                className="mt-5"
                                            >
                                                <Image
                                                    src={mail_icon}
                                                    alt="no image"
                                                    className="img-fluid"
                                                    style={{ marginRight: 10 }}
                                                />
                                                <span
                                                    className="white-color"
                                                    style={{
                                                        textTransform: 'none',
                                                    }}
                                                >
                                                    support@dufther.com
                                                </span>
                                            </a>
                                        )}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-4">
                            <div className="footer-mail d-flex align-items-center justify-content-end">
                                <div className="footer-social d-flex justify-content-center align-items-center d-md-none w-100 my-3">
                                    <div className="footer-social-icon d-flex align-items-center justify-content-end w-100">
                                        <span className="white-color">
                                            Follow Us :{' '}
                                        </span>
                                        <ul className="d-flex align-items-center ">
                                            <li>
                                                <a
                                                    href="https://www.facebook.com/Dufther"
                                                    target="_blank"
                                                >
                                                    <Image
                                                        src={facebook_icon}
                                                        alt="no image"
                                                        className="img-fluid social-svg"
                                                    />
                                                    <Image
                                                        src={
                                                            facebook_icon_hover
                                                        }
                                                        alt="no image"
                                                        className="img-fluid social-hover-svg"
                                                    />
                                                </a>
                                            </li>
                                            <li>
                                                <a
                                                    href="https://www.linkedin.com/company/dufther"
                                                    target="_blank"
                                                >
                                                    <Image
                                                        src={linkedin_icon}
                                                        alt="no image"
                                                        className="img-fluid social-svg"
                                                    />
                                                    <Image
                                                        src={
                                                            linkedin_icon_hover
                                                        }
                                                        alt="no image"
                                                        className="img-fluid social-hover-svg"
                                                    />
                                                </a>
                                            </li>
                                            <li>
                                                <a
                                                    href="https://www.youtube.com/channel/UCkMtaYSgsNNlD5V2gpF5wQg/?guided_help_flow=5"
                                                    target="_blank"
                                                >
                                                    <Image
                                                        src={youtube_icon}
                                                        alt="no image"
                                                        className="img-fluid social-svg"
                                                    />
                                                    <Image
                                                        src={youtube_icon_hover}
                                                        alt="no image"
                                                        className="img-fluid social-hover-svg"
                                                    />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="footer-copyright-logo-mobile w-100">
                                        <Image
                                            src={footer_img_2}
                                            style={{
                                                width: '213px',
                                                height: '100px',
                                            }}
                                            className="img-fluid"
                                            alt="footer-startup"
                                        />
                                    </div>
                                </div>
                                {windowSize > 500 && (
                                    <a href="mailto:support@dufther.com">
                                        <Image
                                            src={mail_icon}
                                            alt="no image"
                                            className="img-fluid"
                                        />
                                        <span className="white-color">
                                            support@dufther.com
                                        </span>
                                    </a>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="footer-copyright">
                        <div className="d-flex flex-wrap align-items-center justify-content-between  ul-flex">
                            <div className="footer-copyright-logo footer-mb-3-first d-none d-sm-block">
                                <Image
                                    src={footer_img_2}
                                    style={{ width: '213px', height: '54px' }}
                                    className="img-fluid"
                                    alt="footer-startup"
                                />
                            </div>
                            <span className="footer-mb-3-sec">
                                © {year}&nbsp;{' '}
                                <button
                                    type="button"
                                    className="btn btn-link px-0 text-white mb-1"
                                    onClick={handleShow}
                                >
                                    Version 2.3
                                </button>{' '}
                            </span>
                            <a
                                href="https://bluesparx.in/"
                                className="footer-mb-3-third"
                                target="_blank"
                            >
                                BluesparX Technologies
                            </a>
                        </div>
                    </div>
                </div>
            </footer>
            <Modal
                centered
                show={show}
                onHide={handleClose}
                className="versionmodal"
            >
                <Modal.Header className="modal_header">
                    <p className="modal-title modal_header_txt">
                        Version History
                    </p>
                    <button
                        type="button"
                        className="close"
                        onClick={() => setShow(false)}
                    >
                        {/* <span aria-hidden="true">X</span> */}
                        <span className="sr-only">x</span>
                    </button>
                </Modal.Header>
                <Modal.Body>
                    <Table striped responsive hover size="sm">
                        <thead>
                            <tr>
                                <th>Ver</th>
                                <th>Release Dt.</th>
                                <th>Change Logs</th>
                            </tr>
                        </thead>
                        <tbody>
                            {versionData?.map((data, index) => (
                                <tr key={index}>
                                    <td style={{ width: '80px' }}>
                                        {data.version}
                                    </td>
                                    <td style={{ width: '120px' }}>
                                        {data.release}
                                    </td>
                                    <td>{data.description}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default Footer
