'use client'
import Image from 'next/image'
import './aboutUs.scss'
import vision_img from '../../../public/svg/vision.svg'
import about_us_image from '../../../public/svg/about-us-image.svg'
import mission_img from '../../../public/svg/mission.svg'
import team_image from '../../../public/svg/team-image.svg'
import about_company_image from '../../../public/images/about-company-image.png'
import { useState } from 'react'

const BuildingSolution = () => {
    const [navId, setNavId] = useState('team')

    return (
        <>
            {/* Building Soultion */}
            <div className="building-solutions ">
                <div className="main-title">
                    <h1>
                        Building a
                        <span className="ornage-color mrl"> solution</span>
                        is an art, and
                        <br /> we are still in the process of
                        <br /> crafting our masterpiece..
                    </h1>
                </div>
            </div>
            {/* About Us section */}

            <div className="about-us">
                <div className="main-title">
                    <h2 className="border-orange-color">About Us</h2>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-5 col-lg-4 col-xl-4 text-center">
                            <Image src={about_us_image} alt="no image" />
                        </div>
                        <div className="col-12 col-md-7 col-lg-8 col-xl-8">
                            <p>
                                We, at <b>Bluesparx Technologies</b> believe
                                that Dufther will create a Unique Recruitment
                                Model to bridge the Demand-Supply Gap in our
                                country.
                                <span className="break-margin">
                                    We focus on enhancing and leveraging the
                                    digital capabilities so that Organizations
                                    can focus on building their core expertise,
                                    and expand market footprints
                                </span>
                                <span className="break-margin">
                                    Formal education and supplementary training
                                    programs are the key ingredients for skilled
                                    and deployable Human capital. At Dufther, we
                                    are building an Ecosystem, where the focus
                                    is on providing multiple options to hire
                                    qualified Professionals with faster
                                    turnaround and flexibility, to meet your
                                    Business Goals.
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            {/* Team Group section */}
            <div className="about-group-section">
                <div className="row ">
                    <div className="col-4 col-xl-3 nav-section">
                        <ul>
                            <li
                                className={`${
                                    navId === 'team' ? 'active-nav' : ''
                                }`}
                            >
                                <h6>Team</h6>
                            </li>
                            <li
                                className={`${
                                    navId === 'our-values' ? 'active-nav' : ''
                                }`}
                            >
                                <h6>Our Values</h6>
                            </li>
                            <li
                                className={`${
                                    navId === 'company-details'
                                        ? 'active-nav'
                                        : ''
                                }`}
                            >
                                <h6>Registration Details</h6>
                            </li>
                        </ul>
                    </div>
                    <div
                        className="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-9 nav-route "
                        // onScroll={(e) => handleScroll(e)}
                    >
                        <div className="row  route-details child" id="team">
                            <div className="main-title justify-content-start align-items-start">
                                <h2 className="border-orange-color">Team</h2>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-6">
                                <p>
                                    We are experienced Professionals with
                                    expertise in multiple domains on the
                                    Technology, and Engineering Service Sectors.
                                    Our Hands-On knowledge on Hiring, and
                                    Software development has converged in making
                                    the Product that is easy to use, with Robust
                                    Features and Capabilities.
                                </p>
                                <p>
                                    We are always on the lookout to scale-up our
                                    core team, and if you are excited to be a
                                    part of the Dufther Journey, we will be
                                    delighted to connect and explore
                                    possibilities.
                                </p>
                                <p>
                                    We look forward to meeting professionals who
                                    want to shake up the existing status quo,
                                    and are hungry for making an impact towards
                                    a larger cause.
                                </p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-4 img-fluid ">
                                <Image src={team_image} alt="no image" />
                            </div>
                        </div>
                        <div
                            className="row route-details child "
                            id="our-values"
                        >
                            <div className="main-title justify-content-start align-items-start">
                                <h2 className="border-orange-color">
                                    Our Values
                                </h2>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-6">
                                <p>
                                    We take pride in our Team who have embodied
                                    the spirit of delivering Quality, a can-do
                                    attitude and a tireless pursuit of getting
                                    results for the efforts logged.
                                </p>
                                <p>
                                    Since we are storing user information, we
                                    have put all mechanisms in place to ensure
                                    that Data is protected, with minimal 3rd
                                    party integrations. Any proprietary or
                                    internal data of the Agencies or any other
                                    partner companies are fully protected from
                                    getting exposed with control points.
                                </p>
                                <p>
                                    For us, the Bigger picture is to develop an
                                    innovative approach to solve the Demand vs
                                    Supply Gaps across industries.
                                </p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-4 card-fluid ">
                                <div className="our-values-card ">
                                    <Image
                                        src={vision_img}
                                        alt="no image"
                                        className="img-fluid social-hover-svg"
                                    />
                                    <h6 className="mt-2 ml-2">Vision</h6>
                                    <p className="reduce-line-height">
                                        To transform the Future of Hiring
                                    </p>
                                </div>
                                <div className="our-values-card">
                                    <Image
                                        src={mission_img}
                                        alt="no image"
                                        className="img-fluid social-hover-svg"
                                    />
                                    <h6 className="mt-2">Mission</h6>
                                    <p className="reduce-line-height">
                                        Addressing Skill shortages through
                                        creative solutions and providing
                                        industries with employable workforce
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row  child" id="company-details">
                            <div className="main-title justify-content-start align-items-start">
                                <h2 className="border-orange-color">
                                    Company Details
                                </h2>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-6">
                                <p>
                                    BluesparX Technologies is a Private Limited
                                    Entity based from Mumbai, Maharashtra.
                                </p>
                                <p>
                                    Our Company Identification Number is
                                    U72900MH2020PTC340744, as registered under
                                    the Ministry of corporate Affairs,
                                    Government of India.
                                </p>
                                <p>
                                    Presently the entity is operating as a
                                    Start-up firm (DIPP 74725), and the product
                                    name “Dufther” is registered under the Trade
                                    Marks Act, 1999.
                                </p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-4 img-fluid about-company-img">
                                <Image
                                    src={about_company_image}
                                    alt="no image"
                                    height={425}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default BuildingSolution
