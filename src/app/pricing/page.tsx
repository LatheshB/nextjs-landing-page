'use client'
import React, { useEffect } from 'react'
import Pricing from '@/component/Pricing'

const Page = () => {
    useEffect(() => {
        window.scrollTo(0,0)
    },[])
    return (
        <>
            <Pricing />
        </>
    )
}

export default Page
