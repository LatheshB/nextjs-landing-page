'use client'
import React, { useEffect } from 'react'
import CollaborationPlatform from '@/component/CollaborationPlatform'
import TalentBridge from '@/component/TalentBridge'
import BecomeSolution from '@/component/BecomeSolution'
import ReachabilityAssistance from '@/component/ReachabilityAssistance'

const Page = () => {
    
    useEffect(() => {
        window.scrollTo(0,0)
    },[])

    return (
        <>
            <CollaborationPlatform />
            <TalentBridge />
            <BecomeSolution />
            <ReachabilityAssistance />
        </>
    )
}

export default Page
