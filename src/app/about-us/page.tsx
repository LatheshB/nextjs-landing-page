'use client'
import React, { useEffect } from 'react'
import AboutUs from '@/component/AboutUs'

const Page = () => {
    
    useEffect(() => {
        window.scrollTo(0,0)
    },[])

    return (
        <>
            <AboutUs />
        </>
    )
}

export default Page
